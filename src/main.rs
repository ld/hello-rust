use std::io;
use std::cmp::Ordering;
use rand::Rng;

/**
我的第一个 Rust 程序
*/
fn main() {

    let secret_number = rand::thread_rng().gen_range(1..101);

    loop {
        println!("Enter your guessed: ");

        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("{} < {} Too small!", guess , secret_number),
            Ordering::Greater => println!("{} > {} Too big!", guess, secret_number),
            Ordering::Equal => {
                println!("You win ({})!", guess);
                break;
            }
        }
    }
}

/**
最大公约数
 */
fn gcd(mut n: u64, mut m: u64) -> u64 {
    assert!(n != 0 && m != 0);
    while (m != 0) {
        if m < n {
            let t = m;
            m = n;
            n = t;
        }
        m = m % n;
    }
    return n;44
}
