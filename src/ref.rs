#[warn(unused_variables)]

fn main() {
    let s1 = String::from("Hello Rust");
    let s1 = "";

    let len = len(&s1);

    println!("The length of '{}' is {}.", s1, len);
}

fn len(s: &str) -> usize {
    s.len()
}